## 🚀 Quick Start

### How to run the Tests locally

- clone the repository locallly
- run the following command below in your terminal to install the dependencies
  
```bash
npm install
```

### How to kickstart the test

```bash
npx playwright test
```

### How to kickstart the test using npm scripts as configured in the package.json

```bash
npm run test
```

### How to kickstart the test using Playwright UI

```bash
npx playwright test --ui
```

### To show Test report

```bash
npx playwright show-report
```

### To test using Chrome

```bash
npx playwright test --project=chromium
```

### To run specific test file

```bash
npx playwright test file-name --project=chromium
```

### How to Run the Tests on your CI

- create a file called .gitlab-ci.yml on the root folder of your repository, the assumption is that you will be using gitlab to test your project.
- in your deployment file, you will need the official docker image for playwright, use the latest tag as your image tag.
- the pipeline will have a single stage, it does not need multiple stages, some applications may need building, deploying stages etc.
- cache is used for specifying directories to cache between pipeline runs to speed up future runs, it skips the step of installing the dependencies afresh every time a push is made to the branch.
- before_script is used to install dependencies in a clean way, it is faster and better than npm install.
- test is the  job that runs the actual tests.
- only is used to ensure the pipeline only runs when a push is made on the specified branch.

The pipeline is configured as seen below
  
```bash
image: mcr.microsoft.com/playwright:latest

stages:
  - test

cache:
  paths:
    - node_modules/

before_script:
  - npm ci  

test:
  stage: test
  script:
    - npx playwright install-deps  
    - npm run test  
  artifacts:
    paths:
      - test-results/  
    when: always
  only:
    - main
```
