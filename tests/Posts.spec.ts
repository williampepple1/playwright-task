

const { test, expect } = require('@playwright/test');

test.describe('API Grouping for Test Cases', async() => {
  const baseUrl = 'https://jsonplaceholder.typicode.com';
  let totalNumberOfPosts = 0;
  let postId: number | null = null;


  const payload = {
    title: "Foods in Japan",
    body: "Japan is known for its rich culinary tradition and a variety of foods that range from everyday staples to gourmet delicacies, they include sushi,Sashimi, Ramen, Yakitori",
    userId: 101
  };

  const updatePayload = {
    body: "There are some Regional Specialties in Japan like Kobe beef, Hokkaido Seafood, Mochi, Dorayaki, Senbei, Taiyaki",

  };


  test('API Tests', async ({request}) => {


    await test.step('Get Total Number of Posts and Store in a Variable', async () => {
    
      const response = await request.get(`${baseUrl}/posts`);
      expect(response.status()).toBe(200);
      const postsData = await response.json();
  
      totalNumberOfPosts = postsData.length; 
    
    });
  
    await test.step('Create a New post and store its ID in a variable', async() => {
      const response = await request.post(`${baseUrl}/posts`, {
        data: payload,
      })
  
      const postsData = await response.json();
      expect(postsData.userId).toBe(payload.userId);
      postId = postsData.id;
    
    })
  
  
    await test.step('Get the Created Post by ID', async() => {
      const response = await request.get(`${baseUrl}/posts/${postId}`)
      expect(response.status()).toBe(200);
  
      const postsData = await response.json();
      expect(postsData.id).toBe(postId);
  
    
    })
  
  
    await test.step('Replace some field in the created Post using Patch ', async () => {
      
      const response = await request.patch(`${baseUrl}/posts/${postId}`, {
        data: updatePayload,
      });
      expect(response.status()).toBe(200);
    
      const updatedData = await request.get(`${baseUrl}/posts/${postId}`)
      const postsData = await updatedData.json();
      expect(postsData.body).toEqual(updatedData.body);
    
    });
    
  
  
   await test.step('Delete Post created by its ID ', async () => {
      
      const response = await request.delete(`${baseUrl}/posts/${postId}`);
      expect(response.status()).toBe(200);
  
    
      const ConfirmDeletion = await request.get(`${baseUrl}/posts/${postId}`)
      expect(ConfirmDeletion.status()).toBe(404);
    
    });
    
  
  
    await test.step('Read Final Number of Posts and Store in a Variable', async () => {
      
      const response = await request.get(`${baseUrl}/posts`);
      expect(response.status()).toBe(200);
      
      const postsData = await response.json();
    
      const totalPostsCount = postsData.length;
      expect(totalPostsCount).toEqual(totalNumberOfPosts)
    
    });
    
  })

 });

 

  





