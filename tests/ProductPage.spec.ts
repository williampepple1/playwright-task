import { test, expect } from '@playwright/test';
import ProductPage from '../pageObjects/ProductPage'


test.beforeEach('User Login',async ({ page }) => {
    const productPage = new ProductPage(page)
    await productPage.visitUrl()
    await productPage.login()
})



test('Sort the list in Ascending Order', async ({page}) => {
  const productPage = new ProductPage(page)
  await productPage.verifyLandingPage()
  await productPage.checkListSortAscendingOrder()
    
})


test('Sort the list in Descending Order', async ({page}) => {
 const productPage = new ProductPage(page)
 await productPage.verifyLandingPage()
 await productPage.checkListSortDescendingOrder()
    
})
