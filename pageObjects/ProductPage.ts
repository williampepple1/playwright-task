import {expect, Page, Locator} from '@playwright/test'

export default class ProductPage {
    readonly page: Page;
    readonly userName: Locator;
    readonly password: Locator;
    readonly loginButton: Locator;
    readonly itemName: Locator;
    readonly productSortContainer: Locator;
    readonly appName: Locator;

    constructor(page: Page) {
        this.page = page;
        this.userName = page.locator("#user-name");
        this.password = page.locator("#password");
        this.loginButton = page.locator("[value='Login']");
        this.itemName = page.locator('.inventory_item_name');
        this.productSortContainer = page.locator(".product_sort_container");
        this.appName = page.locator(".app_logo");
    }

    async visitUrl (){
        await this.page.goto('https://www.saucedemo.com/');
    }

    async login(){
        await this.userName.fill("standard_user");
        await this.password.fill("secret_sauce");
        await this.loginButton.click();
    }

    async verifyLandingPage(){
        expect(await this.appName.textContent()).toMatch("Swag Labs")

    }

    async checkListSortAscendingOrder(){
        await this.productSortContainer.selectOption('az');
        const itemNamesList = await this.itemName.allTextContents();
        const sortedItemNames = [...itemNamesList].sort((a, b) => a.localeCompare(b));
        expect(itemNamesList).toStrictEqual(sortedItemNames)
    }

    async checkListSortDescendingOrder(){
        await this.productSortContainer.selectOption('za');
        const itemNamesList = await this.itemName.allTextContents();
        const sortedItemNames = [...itemNamesList].sort((a, b) => b.localeCompare(a));
        expect(itemNamesList).toStrictEqual(sortedItemNames)
    }
}