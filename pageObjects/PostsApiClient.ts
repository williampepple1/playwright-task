import { APIRequestContext, APIResponse } from '@playwright/test'


export default class PostsApiClient {
    private request: APIRequestContext;
    private baseUrl: string;

    constructor(request: APIRequestContext, baseUrl: string) {
        this.request = request;
        this.baseUrl = baseUrl;
    }


    async getAllPosts(): Promise<APIResponse> {
        return await this.request.get(`${this.baseUrl}/posts`);
    }

    async createPost(payload: object): Promise<APIResponse> {
        return await this.request.post(`${this.baseUrl}/posts`, {
            data: payload
        });

     }

     async getPostById(postId: number): Promise<APIResponse> {
        return await this.request.get(`${this.baseUrl}/posts/${postId}`);
    }

    async updatePostById(postId: number): Promise<APIResponse> {
        return await this.request.put(`${this.baseUrl}/posts/${postId}`);
    }

async deletePostById(postId: number): Promise<APIResponse> {
        return await this.request.delete(`${this.baseUrl}/posts/${postId}`);
    }
}